from django.forms import ModelForm
from projects.models import Project
from tasks.models import Task


class CreateProject(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]


class CreateProjectTask(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
