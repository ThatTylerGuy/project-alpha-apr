from django.shortcuts import render, get_list_or_404
from projects.models import Project
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject, CreateProjectTask


# Create your views here.
@login_required
def project_instance(request):
    project = get_list_or_404(Project)
    context = {"project_inst": project}
    return render(request, "projects/projects.html", context)


def redirect_to_projects(request):
    return redirect(project_instance)


@login_required
def Create_Project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def Create_Project_Task(request):
    if request.method == "POST":
        form = CreateProjectTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectTask()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)
