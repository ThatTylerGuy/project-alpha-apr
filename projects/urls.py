from django.urls import path
from projects.views import project_instance, Create_Project
from tasks.views import ProjectTasks

urlpatterns = [
    path("<int:id>/", ProjectTasks, name="show_project"),
    path("create/", Create_Project, name="create_project"),
    path("", project_instance, name="list_projects"),
]
