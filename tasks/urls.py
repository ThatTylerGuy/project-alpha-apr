from django.urls import path
from projects.views import Create_Project_Task
from tasks.views import MyTasks

urlpatterns = [
    path("create/", Create_Project_Task, name="create_task"),
    path("mine/", MyTasks, name="show_my_tasks"),
]
