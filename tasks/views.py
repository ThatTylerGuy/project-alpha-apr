from django.shortcuts import render, get_object_or_404
from tasks.models import Task, Project
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def ProjectTasks(request, id):
    project_tasks = Task.objects.filter(assignee=request.user)
    project = get_object_or_404(Project, id=id)
    context = {"project_tasks": project_tasks, "project_in": project}
    return render(request, "tasks/tasks.html", context)


@login_required
def MyTasks(request):
    assignedTasks = Task.objects.filter(assignee=request.user)
    project = Project.objects.filter(owner=request.user)
    context = {"my_tasks": assignedTasks, "project_in": project}
    return render(request, "tasks/mine.html", context)
